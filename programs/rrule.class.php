<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/vendor/autoload.php';


class LibRrule_RRule extends \RRule\RRule {
    
    /**
     * Human readable with options for Ovidentia
     */
    public function humanReadable(array $opt = array()) {
        
        $opt['locale'] = bab_getLanguage();
        
        return bab_getStringAccordingToDatabase(parent::humanReadable($opt), 'UTF-8');
    }
}



/**
 * @url https://github.com/rlanvin/php-rrule
 */
class Func_Rrule extends bab_functionality
{
    public function getDescription() {
        return 'icalendar RRULE';
    }
    
    /**
     * Compose RRULE string from the form fields values
     * @see ORM_RruleField
     * @param array $input
     * @return string
     */
    public function formInput(Array $input) {
        
        if (empty($input['FREQ'])) {
            return '';
        }
        
        $rrule = new LibRrule_RRule($input);
        return $rrule->__toString();
    }
    
    /**
     * Decompose the RRULE string for the form fields
     * @see ORM_RruleField
     * @param string $str
     * @return array
     */
    public function formOutput($str) {
        $rrule = new LibRrule_RRule($str);
        return $rrule->getRule();
    }
    
    /**
     * Get the RRule iterator from a rrule string or form input array
     * @param mixed $mixedValue
     * @param DateTime | string $dtstart
     * @return RRule\RRule
     */
    public function getRrule($mixedValue, $dtstart = null) {
        return new LibRrule_RRule($mixedValue, $dtstart);
    }
}